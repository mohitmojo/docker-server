const express = require("express");
const cors = require("cors");
const app = express();

app.use(cors());

app.get("/testApi", (req, res) => {
  res.send("Hello from node. Api is working properly.");
});

app.listen(8080, () => {
  console.log("Listening on port 8080");
});
